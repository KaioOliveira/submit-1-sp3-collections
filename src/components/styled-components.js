import { Card } from "antd";
import styled from "styled-components";
import { motion } from "framer-motion";
import { Link } from "react-router-dom";

export const StyledHeader = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

export const StyledCharacter = styled(motion.div)`
  display: flex;
  padding: 20px;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const StyledCard = styled(Card)`
  margin: 10px;
  width: 240px;
  height: 324px;
`;

export const StyledList = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;
export const StyledControl = styled.div`
  padding: 10px;
  max-width: 500px;
  display: flex;
  width: 100%;
  justify-content: space-between;
`;
export const TopBarLinks = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

export const DefaultButton=styled.button`
  background-color:black;
  color:white;
  margin:20px 0px;
  padding:1em;
  &:hover{
    color:blue;
  };
`;

export const TopBar = styled.div`
  background-color: white;
  width: 100%;
  position: fixed;
  top: 0;
  padding: 5px;
  z-index: 10;
`;

export const StyledLink = styled(Link)`
  margin: 40px;
`;
