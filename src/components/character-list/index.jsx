import React from "react";
import { Card } from "antd";
import {
  StyledCharacter,
  StyledCard,
  StyledHeader,
  StyledList,
} from "../styled-components";
import { motion } from "framer-motion";
import "./index.css";
const CharacterList = ({ characters, header, onSelect = () => {} }) => {
  return (
    <StyledCharacter >
      <StyledHeader>{header}</StyledHeader>
      <StyledList>
        {characters &&
          characters.map(({ name, image, species, type }, key) => (
            <motion.div whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.8 }}>
              <StyledCard
                key={key}
                cover={
                  <img
                    alt="example"
                    src={image}
                    onClick={() => {
                      onSelect({ name, image, species, type });
                    }}
                  />
                }
              >
                <Card.Meta title={name} description={species} />
              </StyledCard>
            </motion.div>
          ))}
      </StyledList>
    </StyledCharacter>
  );
};
export default CharacterList;